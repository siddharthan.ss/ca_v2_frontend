import { Router } from '@angular/router';
import { Component } from '@angular/core';

import { GlobalState } from '../../../global.state';
import { LoginService } from 'app/pages/login/login.service';

@Component({
  selector: 'ba-page-top',
  templateUrl: './baPageTop.html',
  styleUrls: ['./baPageTop.scss'],
  providers: [LoginService],
})
export class BaPageTop {

  isScrolled: boolean = false;
  isMenuCollapsed: boolean = false;

  constructor(
    private _state: GlobalState,
    private loginService: LoginService,
    private router: Router,
    ) {
    this._state.subscribe('menu.isCollapsed', (isCollapsed) => {
      this.isMenuCollapsed = isCollapsed;
    });
  }

  toggleMenu() {
    this.isMenuCollapsed = !this.isMenuCollapsed;
    this._state.notifyDataChanged('menu.isCollapsed', this.isMenuCollapsed);
    return false;
  }

  scrolledChanged(isScrolled) {
    this.isScrolled = isScrolled;
  }

  handleSignOut() {
    this.loginService.logout();
    this.router.navigateByUrl('login');
  }
}
