

export class ProgrammeModel {
    id: number;
    acronym: string;
    name: string;
}
