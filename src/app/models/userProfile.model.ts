import { StaffProfile } from './staffProfile.model';
import { Avatar } from './avatar';
import { StudentProfile } from './studentProfile.model';

export interface UserProfile {
    id: number;
    avatar: Avatar;
    last_login: string;
    is_superuser: boolean;
    email: string;
    is_staff_account: boolean;
    has_filled_data: boolean;
    is_approved: boolean;
    is_verified: boolean;
    groups: number[];
    user_permissions: number[];
    staff_profile: StaffProfile;
    student_profile: StudentProfile;
}
