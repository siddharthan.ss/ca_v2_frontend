

export interface Avatar {
    full_size: string;
    thumbnail: string;
    medium_square_crop: string;
    small_square_crop: string;
}
