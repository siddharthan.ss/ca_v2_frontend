
export interface StaffProfile {
    id: number;
    first_name: string;
    middle_name: string;
    last_name: string;
    gender: string;
    dob: string;
    designation: string;
    degree: string;
    specialization: string;
    temporary_address: string;
    permanent_address: string;
    phone_number: string;
    user: number;
    department: number;
    qualification: number;
}
