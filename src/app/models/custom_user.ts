import { Avatar } from './avatar';

export interface CustomUserModel {
    id: number;
    has_filled_data: boolean;
    is_approved: boolean;
    is_verified: boolean;
    email: string;
    avatar: Avatar;
}
