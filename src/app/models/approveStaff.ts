import { CustomUserModel } from './custom_user';

export interface ApproveStaff {
    id: number;
    user: number;
    first_name: string;
    middle_name: string;
    last_name: string;
    designation: string;
    custom_user: CustomUserModel;
}
