

interface ResponseData {
    response_code: string;
    response_message: string;
    response_explanation: string;
}
