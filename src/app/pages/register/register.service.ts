import { Injectable } from '@angular/core';
import { Headers, Http, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { registerURL } from '../../../environments/api_urls';


@Injectable()
export class RegisterService {  // Oauth Login EndPointUrl to web API

  constructor(public http: Http) {}

  register(username: string, password: string, staff:boolean): Observable<JSON> {


      const body = 'email=' + username + '&password=' + password + '&is_staff_account=' + staff ;
    console.log('registered');
    const headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    return this.http.post(registerURL , body, {
        headers,
    }).map(this.handleData)
    .catch(this.handleError);
  }

  private handleData(res: Response) {
    const body = res.json();
    return body;
  }

  private handleError (error: Response): Observable<signup>{
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    // const errMsg = (error.message) ? error.message :
    // error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.log(error); // log to console instead
    console.log(error.json()); // log to console instead
    console.log('return form service');
    return Observable.throw(error.json());
  }
}

interface signup
{
  email:string[];
  password:string[];

}
interface signup1
{
  email:string[];
}
