import {Component} from '@angular/core';
import {FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import {EmailValidator, EqualPasswordsValidator} from '../../theme/validators';
import {RegisterService} from "./register.service";
import {passBoolean} from "protractor/built/util";

@Component({
  selector: 'register',
  templateUrl: './register.html',
  styleUrls: ['./register.scss']
})
export class Register {

  public form:FormGroup;
  // public name:AbstractControl;
  public email:AbstractControl;
  public password:AbstractControl;
  public repeatPassword:AbstractControl;
  public passwords:FormGroup;
  public staffchk:AbstractControl;
  public checkBoxValue;
  public options;
  public error:signup;
  public emailerr:boolean;
  public passerr:boolean;
  public toggleS:boolean;
  public submitted:boolean = false;
  public errorbool:boolean;

  constructor(fb:FormBuilder,
  private registerservice:RegisterService)
  {
    this.errorbool=false;
    this.toggleS=true;

    this.form = fb.group({
      // 'name': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'email': ['', Validators.compose([Validators.required, EmailValidator.validate])],
      'passwords': fb.group({
        'password': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
        'repeatPassword': ['', Validators.compose([Validators.required, Validators.minLength(4)])]
      }, {validator: EqualPasswordsValidator.validate('password', 'repeatPassword')})
    });

    // this.name = this.form.controls['name'];
    this.email = this.form.controls['email'];
    this.passwords = <FormGroup> this.form.controls['passwords'];
    this.password = this.passwords.controls['password'];
    this.repeatPassword = this.passwords.controls['repeatPassword'];
    this.emailerr=false;
    this.passerr=false;


  }

  toggle(){
    this.toggleS = !this.toggleS;
    console.log(this.toggleS);
  }

  public onSubmit(values:Object):void
  {
    this.errorbool=false;
    this.emailerr=false;
    this.passerr=false;

    if (this.form.valid) {
      console.log('success!!');
      console.log(this.email.value);
      console.log(this.password.value);
      console.log(this.checkBoxValue);
      console.log(this.toggleS);
      this.registerservice.register(this.email.value,this.password.value,!this.toggleS).subscribe(
        (response: any) => {
          console.log('success signed up!!1');
          console.log(response);
        },
        (error: signup) => {
          this.errorbool=true;
          this.submitted=false;

          console.log('error:-------');
          console.log(error);
          this.error=error;
          if(this.error.email)
          {
          console.log('email');
          this.emailerr=true;
          } if(this.error.password)
          {
          console.log('pass');
          this.passerr=true;
          }

        },
      );
    }
    if(this.errorbool==false) {
      this.submitted = true;
    }

  }
}

interface signup
{
  email;
  password;

}
