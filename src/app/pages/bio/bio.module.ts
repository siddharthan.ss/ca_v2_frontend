import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';

import { Bio } from './bio.component';
import { routing }       from './bio.routing';
import {BioService} from "./bio.service";
import {AppTranslationModule} from "../../app.translation.module";


@NgModule({
  imports: [
    CommonModule,
    AppTranslationModule,
    ReactiveFormsModule,
    FormsModule,
    NgaModule,
    routing
  ],
  declarations: [
    Bio
  ],
  providers:
  [BioService],
})
export class BioModule {}
