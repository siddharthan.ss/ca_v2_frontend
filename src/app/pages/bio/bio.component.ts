import {Component} from '@angular/core';
import {FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import {EmailValidator, EqualPasswordsValidator} from '../../theme/validators';
import {BioService} from "./bio.service";
import {passBoolean} from "protractor/built/util";
import {Router} from "@angular/router";


@Component({
  selector: 'bio',
  templateUrl: './bio.html',
  styleUrls: ['./bio.scss'],
})
export class Bio {
  form_student:FormGroup;

  options1;
  obj:any[1];
  error:any;
  submitted:boolean = false;
  errorbool:boolean;
  StaffAccount:boolean;
  first_name:AbstractControl;
  middle_name:AbstractControl;
  last_name:AbstractControl;
  gender:AbstractControl;
  dob;
  designation;
  degree;
  specialization:string='';
  temporary_address:any='';
  permanent_address:any='';
  phone_number;
  user;
  department;
  qualification:'';
  message:string='';
  depsec:dept_sec[];

  private father_name:AbstractControl;
  private father_occupation:AbstractControl;
  father_annual_income:AbstractControl;
  mother_name:AbstractControl;
  mother_occupation:string='';
  mother_annual_income:number=0;
  date_of_joining;
  hosteller_or_dayscholar;
  entry_type;
  caste;
  community;
  aadhaar_number;
  department_section:AbstractControl;
  batch:AbstractControl;
  programme:AbstractControl;
  roll_no:AbstractControl;

  constructor(fb: FormBuilder,
              private bioService: BioService,
              private router: Router,) {
    this.errorbool = false;

    bioService.self().subscribe(
      (response: any) => {
        console.log('self_service');
        console.log(response);
        this.obj = response[0];
        console.log(this.obj);
        console.log('staff account');
        console.log(this.obj.is_staff_account);
        this.StaffAccount = this.obj.is_staff_account;
        if(this.obj.has_filled_data) {
          // this.router.navigateByUrl('pages/dashboard');
        }
        this.user = this.obj.id;
      },
      (error: any) => {
        console.log(error);
        this.error = error;
      },
    );
    this.form_student = fb.group({
      'first_name': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'last_name': ['', Validators.compose([Validators.required, Validators.minLength(1)])],
      'middle_name': ['', Validators.compose([ Validators.minLength(2)])],
      'roll_no': ['', Validators.compose([Validators.required, Validators.minLength(7),Validators.maxLength(7)])],
      // 'gender': ['', Validators.compose([Validators.required])],
    });

    this.first_name = this.form_student.controls['first_name'];
    this.middle_name = this.form_student.controls['middle_name'];
    this.last_name = this.form_student.controls['last_name'];
    this.roll_no = this.form_student.controls['roll_no'];
    this.batch = this.form_student.controls['batch'];
    // this.gender = this.form_student.controls['gender'];





    // {
    //   "roll_no": "12123",
    //   "first_name": "dsd",
    //   "middle_name": "hkj",
    //   "last_name": "jkjlk",
    //   "gender": "M",
    //   "father_name": "lkm",
    //   "father_occupation": "lklkm",
    //   "father_annual_income": 3.0,
    //   "mother_name": "kllkm",
    //   "mother_occupation": "kmlkm",
    //   "mother_annual_income": 4.0,
    //   "temporary_address": "kmkl",
    //   "permanent_address": "klkm",
    //   "dob": "1999-09-02",
    //   "date_of_joining": "1999-09-02",
    //   "hosteller_or_dayscholar": "dayscholar",
    //   "entry_type": "regular",
    //   "phone_number": "ml",
    //   "caste": "ml",
    //   "community": "OC",
    //   "aadhaar_number": "1212121",
    //   "user": 2,
    //   "department_section": 1,
    //   "batch": 1,
    //   "programme": 1
    // }

    // bioService.get_department_section().subscribe(
    //   (response: any) => {
    //     console.log('department section all');
    //     console.log(response);
    //     this.depsec=response;
    //
    //     console.log('dep_');
    //     console.log(this.depsec);
    //
    //   },
    //   (error: any) => {
    //     console.log(error);
    //     this.error=error;
    //   },
    // );

    this.depsec=[
      {
        "ds": "CSE UG ( A )",
        "id": 1
      },
      {
        "ds": "CSE PG ( A )",
        "id": 2
      },
      {
        "ds": "CSE PhD ( A )",
        "id": 3
      },
      {
        "ds": "IT UG ( A )",
        "id": 4
      },
      {
        "ds": "IT PG ( A )",
        "id": 5
      },
      {
        "ds": "IT PhD ( A )",
        "id": 6
      },
      {
        "ds": "MECH UG ( A )",
        "id": 7
      },
      {
        "ds": "MECH UG ( B )",
        "id": 8
      },
      {
        "ds": "MECH PG ( A )",
        "id": 9
      },
      {
        "ds": "MECH PhD ( A )",
        "id": 10
      },
      {
        "ds": "PROD UG ( A )",
        "id": 11
      },
      {
        "ds": "PROD PG ( A )",
        "id": 12
      },
      {
        "ds": "PROD PhD ( A )",
        "id": 13
      },
      {
        "ds": "CIVIL UG ( A )",
        "id": 14
      },
      {
        "ds": "CIVIL UG ( B )",
        "id": 15
      },
      {
        "ds": "CIVIL PG ( A )",
        "id": 16
      },
      {
        "ds": "CIVIL PhD ( A )",
        "id": 17
      },
      {
        "ds": "EEE UG ( A )",
        "id": 18
      },
      {
        "ds": "EEE PG ( A )",
        "id": 19
      },
      {
        "ds": "EEE PhD ( A )",
        "id": 20
      },
      {
        "ds": "ECE UG ( A )",
        "id": 21
      },
      {
        "ds": "ECE PG ( A )",
        "id": 22
      },
      {
        "ds": "ECE PhD ( A )",
        "id": 23
      },
      {
        "ds": "EIE UG ( A )",
        "id": 24
      },
      {
        "ds": "EIE PG ( A )",
        "id": 25
      },
      {
        "ds": "EIE PhD ( A )",
        "id": 26
      },
      {
        "ds": "IBT UG ( A )",
        "id": 27
      },
      {
        "ds": "IBT PG ( A )",
        "id": 28
      },
      {
        "ds": "IBT PhD ( A )",
        "id": 29
      }
    ]
  }



  sub() {

    if(this.StaffAccount) {

      console.log('staff');
      console.log(this.first_name);
      console.log(this.options1);
      console.log(this.middle_name);
      console.log(this.last_name);
      console.log(this.gender);
      console.log(this.dob);
      console.log(this.designation);
      console.log(this.degree);
      console.log(this.specialization);
      console.log(this.temporary_address);
      console.log(this.permanent_address);
      console.log(this.phone_number);
      console.log(this.user);
      console.log(this.department);
      console.log(this.qualification);
      // this.onSubmit();
    }
    else
    {
        console.log('student');
        console.log(this.roll_no);
        console.log(this.first_name);
        console.log(this.options1);
        console.log(this.middle_name);
        console.log(this.last_name);
        console.log(this.gender);
        console.log(this.dob);
        console.log(this.father_name);
        console.log(this.father_occupation);
        console.log(this.father_annual_income);
        console.log(this.mother_name);
        console.log(this.mother_occupation);
        console.log(this.mother_annual_income);
        console.log(this.temporary_address);
        console.log(this.permanent_address);
        console.log(this.dob);
        console.log(this.date_of_joining);
        console.log(this.hosteller_or_dayscholar);
        console.log(this.entry_type);
        console.log(this.phone_number);
        console.log(this.caste);
        console.log(this.community);
        console.log(this.aadhaar_number);
        console.log(this.user);
        console.log(this.department_section);
        console.log(this.batch);
        console.log(this.programme);
        this.onStudSubmit();
    }

  }
  onSubmit(values: Object): void {
    this.submitted = true;
    if (this.form_student.valid) {
      // your code goes here
      console.log(values);

    }
  }

  // public onSubmit():void
  // {
  //   this.errorbool=false;
  //
  //     this.bioService.staffreg(
  //       this.first_name,
  //       this.middle_name,
  //       this.last_name,
  //       this.gender,
  //       this.dob,
  //       this.designation,
  //       this.degree,
  //       this.specialization,
  //       this.temporary_address,
  //       this.permanent_address,
  //       this.phone_number,
  //       this.user,
  //       this.department,
  //       this.qualification
  //     ).subscribe(
  //       (response: any) => {
  //         console.log('success staff bio updated !!');
  //         this.submitted=true;
  //         console.log(response);
  //         // this.router.navigateByUrl('pages/dashboard');
  //
  //       },
  //       (error: staff) => {
  //         this.errorbool=true;
  //         this.submitted=false;
  //
  //         console.log('error:-------');
  //         console.log(error);
  //         this.error=error;
  //       },
  //     );
  //
  //   if(this.errorbool==false) {
  //     this.submitted = true;
  //   }
  //
  // }
  protected onStudSubmit():void
  {
    this.errorbool=false;

      this.bioService.studreg(
        this.roll_no,
        this.first_name ,
        this.middle_name ,
        this.last_name ,
        this.gender ,
        this.father_name ,
        this.father_occupation ,
        this.father_annual_income ,
        this.mother_name ,
        this.mother_occupation ,
        this.mother_annual_income ,
        this.temporary_address ,
        this.permanent_address ,
        this. dob ,
        this.date_of_joining ,
        this.hosteller_or_dayscholar ,
        this.entry_type ,
        this.phone_number ,
        this.caste ,
        this.community ,
        this.aadhaar_number ,
        this.user ,
        this.department_section ,
        this.batch ,
        this.programme
      ).subscribe(
        (response: any) => {
          console.log('success student bio updated !!');
          this.submitted=true;
          console.log(response);
          // this.router.navigateByUrl('pages/dashboard');

        },
        (error: student) => {
          this.errorbool=true;
          this.submitted=false;

          console.log('error:');
          console.log(error);
          this.error=error;
        },
      );

    if(this.errorbool==false) {
      this.submitted = true;
    }

  }
}

interface staff {
  first_name;
  middle_name;
  last_name;
  gender;
  dob;
  designation;
  degree;
  specialization;
  temporary_address;
  permanent_address;
  phone_number;
  user;
  department;
  qualification;
}


interface student {
  roll_no;
  first_name;
  middle_name;
  last_name;
  gender;
  father_name;
  father_occupation;
  father_annual_income;
  mother_name;
  mother_occupation;
  mother_annual_income;
  temporary_address;
  permanent_address;
  dob;
  date_of_joining;
  hosteller_or_dayscholar;
  entry_type;
  phone_number;
  caste;
  community;
  aadhaar_number;
  user;
  department_section;
  batch;
  programme;
}



interface dept_sec{

  id:number;
  ds:string;
}
