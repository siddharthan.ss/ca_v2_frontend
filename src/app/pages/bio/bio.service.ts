import { Injectable } from '@angular/core';
import {Headers, Http, Response, URLSearchParams, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Rx';
import {
  registerURL, StaffCreateURL, SelfURL, StudentCreateURL,
  DepartmentSectionURL
} from '../../../environments/api_urls';


@Injectable()
export class BioService {
  accessToken:string;

  constructor(public http: Http) {
    this.accessToken=localStorage.getItem('token');
  }
  staffreg  (
            first_name,
            middle_name,
            last_name,
            gender,
            dob,
            designation,
            degree,
            specialization,
            temporary_address,
            permanent_address,
            phone_number,
            user,
            department,
            qualification
            ): Observable<JSON> {


      const body ='first_name='+
                  first_name+
                  '&middle_name='+
                  middle_name+
                  '&last_name='+
                  last_name+
                  '&gender='+
                  gender+
                  '&dob='+
                  dob+
                  '&designation='+
                  designation+
                  '&degree='+
                  degree+
                  '&specialization='+
                  specialization+
                  '&temporary_address='+
                  temporary_address+
                  '&permanent_address='+
                  permanent_address+
                  '&phone_number='+
                  phone_number+
                  '&user='+
                  user+
                  '&department='+
                  department+
                  '&qualification='+
                  qualification;
    const head = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer '.concat(this.accessToken),
    });
    const requestOptions = new RequestOptions({
      headers: head,
    });
    return this.http.post(StaffCreateURL,body, requestOptions)
      .map(this.handleData)
      .catch(this.handleErrorStaff);

  }

  studreg  (
            roll_no,
            first_name ,
            middle_name ,
            last_name ,
            gender ,
            father_name ,
            father_occupation ,
            father_annual_income ,
            mother_name ,
            mother_occupation ,
            mother_annual_income ,
            temporary_address ,
            permanent_address ,
            dob ,
            date_of_joining ,
            hosteller_or_dayscholar ,
            entry_type ,
            phone_number ,
            caste ,
            community ,
            aadhaar_number ,
            user ,
            department_section ,
            batch ,
            programme,
            ): Observable<JSON> {
              console.log('ds');
              console.log(department_section);


    const body =
      'roll_no='+
      roll_no+


    '&first_name=' +
      first_name +
      '&middle_name=' +
      middle_name +
      '&last_name=' +
      last_name +
      '&gender=' +
      gender +
      '&father_name=' +
      father_name +
      '&father_occupation=' +
      father_occupation +
      '&father_annual_income=' +
      father_annual_income +
      '&mother_name=' +
      mother_name +
      '&mother_occupation=' +
      mother_occupation +
      '&mother_annual_income=' +
      mother_annual_income +
      '&temporary_address=' +
      temporary_address +
      '&permanent_address=' +
      permanent_address +
      '&dob=' +
      dob +
      '&date_of_joining=' +
      date_of_joining +
      '&hosteller_or_dayscholar=' +
      hosteller_or_dayscholar +
      '&entry_type=' +
      entry_type +
      '&phone_number=' +
      phone_number +
      '&caste=' +
      caste +
      '&community=' +
      community +
      '&aadhaar_number=' +
      aadhaar_number +
      '&user=' +
      user +
      '&department_section=' +
      department_section +
      '&batch=' +
      batch +
      '&programme=' +
      programme;
    const head = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer '.concat(this.accessToken),
    });
    const requestOptions = new RequestOptions({
      headers: head,
    });
    return this.http.post(StudentCreateURL,body, requestOptions)
      .map(this.handleData)
      .catch(this.handleErrorStudent);

  }


  private handleErrorStaff (error: Response): Observable<staff>{
    console.log(error);
    console.log(error.json());
    return Observable.throw(error.json());


  }

  private handleErrorStudent (error: Response): Observable<student>{
    console.log(error);
    console.log(error.json());
    return Observable.throw(error.json());


  }

  self(){


    const head = new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '.concat(this.accessToken),
    });
    const requestOptions = new RequestOptions({
      headers: head,
    });
    return this.http.get(SelfURL, requestOptions)
      .map(this.handleData)
      .catch(this.handleError);
  }

  //
  // get_department_section(){
  //
  //   const head = new Headers({
  //     'Content-Type': 'application/json',
  //     'Authorization': 'Bearer '.concat(this.accessToken),
  //   });
  //   const requestOptions = new RequestOptions({
  //     headers: head,
  //   });
  //   return this.http.get(DepartmentSectionURL, requestOptions)
  //     .map(this.handleData)
  //     .catch(this.handleError);
  //
  //
  // }

  private handleData(res: Response) {
    const body = res.json();
    return body;
  }

  private handleError (error: Response): Observable<JSON>{
    console.log(error);
    console.log(error.json());
    return Observable.throw(error.json());
  }
}

interface staff {
  first_name;
  middle_name;
  last_name;
  gender;
  dob;
  designation;
  degree;
  specialization;
  temporary_address;
  permanent_address;
  phone_number;
  user;
  department;
  qualification;
}

interface student {
  roll_no;
  first_name;
  middle_name;
  last_name;
  gender;
  father_name;
  father_occupation;
  father_annual_income;
  mother_name;
  mother_occupation;
  mother_annual_income;
  temporary_address;
  permanent_address;
  dob;
  date_of_joining;
  hosteller_or_dayscholar;
  entry_type;
  phone_number;
  caste;
  community;
  aadhaar_number;
  user;
  department_section;
  batch;
  programme;
}
