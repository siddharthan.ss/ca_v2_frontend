import { Routes, RouterModule }  from '@angular/router';

import { Bio } from './bio.component';


const routes: Routes = [
  {
    path: '',
    component: Bio
  }
];

export const routing = RouterModule.forChild(routes);
