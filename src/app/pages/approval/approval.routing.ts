import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { ApprovalComponent } from 'app/pages/approval/approval.component';

// noinspection TypeScriptValidateTypes
export const routes: Routes = [
  {
    path: '',
    component: ApprovalComponent,
  },
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
