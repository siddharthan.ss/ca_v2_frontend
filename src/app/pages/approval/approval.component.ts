import { baseURL } from '../../../environments/api_urls';
import { ApprovalService } from './approval.service';
import { Component, OnInit } from '@angular/core';
import { ApproveStaff } from '../../models/approveStaff';
import { trigger, state, style, transition, animate} from '@angular/animations';

@Component({
    selector: 'approval',
    templateUrl: './approval.component.html',
      animations: [
    trigger('slideInOut', [
      state('in', style({
        transform: 'translate3d(0, 0, 0)'
      })),
      state('out', style({
        transform: 'translate3d(100%, 0, 0)'
      })),
      transition('in => out', animate('300ms ease-in-out')),
      transition('out => in', animate('300ms ease-in-out'))
    ]),
  ],
    styleUrls: ['./approval.scss'],
})

export class ApprovalComponent implements OnInit {

    checked: boolean = true;

    constructor(
        private approvalService: ApprovalService,
    ) {

    }

    eligibleStaffList: ApproveStaff[];
    imageBaseUrl: string;

    ngOnInit() {
        this.imageBaseUrl = baseURL.slice(0, -1);
        this.approvalService.getEligibleStaffAccounts()
            .subscribe(
            (eligbleStaffList: any) => {
                console.log('got staff accounts');
                console.log(eligbleStaffList);
                this.eligibleStaffList = eligbleStaffList;
                console.log(this.eligibleStaffList);
            },
            (error: any) => {
                console.log('explanation');
                console.log(error.response_explanation);

                // if (error.response_code === 'EMAIL_LINK_EXPIRED') {
                //     this.verificationLinkExpired = true;
                // } else if (error.response_code === 'BAD_VERIFICATION_LINK') {
                //     this.invalidLink = true;
                // }
            },
        );
    }

    ApproveUser(selectedEligibleStaff: ApproveStaff) {
        console.log(selectedEligibleStaff.user);

        this.approvalService.approveCustomUser(selectedEligibleStaff.user, !selectedEligibleStaff.custom_user.is_approved)
            .subscribe(
            (approvedResponse: any) => {
                console.log('got account approved');
                console.log(approvedResponse);
                selectedEligibleStaff.custom_user.is_approved = approvedResponse.is_approved;
            },
            (error: any) => {
                console.log('explanation');
                console.log(error.response_explanation);

                // if (error.response_code === 'EMAIL_LINK_EXPIRED') {
                //     this.verificationLinkExpired = true;
                // } else if (error.response_code === 'BAD_VERIFICATION_LINK') {
                //     this.invalidLink = true;
                // }
            },
        );
    }
  
  
  menuState:string = 'out';

  toggleMenu() {
    this.menuState = this.menuState === 'out' ? 'in' : 'out';
  }


}

