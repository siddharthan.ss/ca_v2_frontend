import { LoginService } from '../login/login.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppTranslationModule } from '../../app.translation.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';

import { routing } from './approval.routing';
import { MenuComponent } from './menu/menu.component';
import { ApprovalComponent } from 'app/pages/approval/approval.component';
import { ApprovalService } from 'app/pages/approval/approval.service';


@NgModule({
  imports: [
    CommonModule,
    AppTranslationModule,
    ReactiveFormsModule,
    FormsModule,
    NgaModule,
    routing,
  ],
  declarations: [
    ApprovalComponent,
    MenuComponent
  ],
  providers: [
    ApprovalService,
    LoginService,
  ],
})
export class ApprovalModule {}
