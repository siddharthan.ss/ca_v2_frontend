import { getStaffsToApproveURL, approveCustomUserURL } from '../../../environments/api_urls';
import { ApproveStaff } from '../../models/approveStaff';
import { Observable } from 'rxjs/Rx';
import { LoginService } from '../login/login.service';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ApprovalService {

    constructor(
        private http: Http,
        private loginService: LoginService,
    ) { }

    getEligibleStaffAccounts() {
        const head = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '.concat(this.loginService.getAccessToken()),
        });
        const requestOptions = new RequestOptions({
            headers: head,
        });

        return this.http.get(getStaffsToApproveURL, requestOptions)
            .map(this.handleData)
            .catch(this.handleError);

    }

    private handleData(result: any) {
        const body = result.json();
        // console.log(body);
        return body;
    }

    private handleError(error: Response) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        // const errMsg = (error.message) ? error.message :
        // error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        // console.error(errMsg); // log to console instead
        console.log(error);
        console.log(error.json());
        return Observable.throw(error.json());
    }

    approveCustomUser(customUserPk: number, approveOrDisapprove: boolean) {

        const data = {
            'is_approved': approveOrDisapprove,
        };
        const body = JSON.stringify(data);
        const head = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '.concat(this.loginService.getAccessToken()),
        });
        const requestOptions = new RequestOptions({
            headers: head,
        });
        

        return this.http.put(approveCustomUserURL.concat(customUserPk.toString()).concat('/'), body, requestOptions)
            .map(this.handleData)
            .catch(this.handleError);
    }


}
