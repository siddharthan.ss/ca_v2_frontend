import { CurriculumComponent } from './curriculum.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AssignPCComponent } from 'app/pages/curriculum/components/assign_pc/assign_pc.component';


const routes: Routes = [
  {
    path: '',
    component: CurriculumComponent ,
    children: [
      { path: 'assign_pc', component: AssignPCComponent },
    ],
  },
];

export const routing = RouterModule.forChild(routes);
