import { LoginService } from '../login/login.service';
import { CurriculumComponent } from './curriculum.component';
import { CommonModule } from '@angular/common';
import { declaredViewContainer } from '@angular/core/src/view/util';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { routing } from './curriculum.routing';


import { AppTranslationModule } from '../../app.translation.module';
import { NgaModule } from '../../theme/nga.module';
import { ProfileService } from '../profile/components/profile.service';
import { AssignPCComponent } from './components/assign_pc/assign_pc.component';
import { AssignPCService } from './components/assign_pc/assign_pc.service';
@NgModule({
    imports: [
        CommonModule,
        routing,
        FormsModule,
        AppTranslationModule,
        ReactiveFormsModule,
        NgaModule,
    ],
    declarations: [
        AssignPCComponent,
        CurriculumComponent,
    ],
    providers: [
        AssignPCService,
        LoginService,
        ProfileService,
    ],
})
export class CurriculumModule {}
