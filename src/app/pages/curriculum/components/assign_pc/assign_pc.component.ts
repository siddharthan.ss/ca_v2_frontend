import { StaffList } from '../../../../models/staffList.model';
import { AssignPCService } from './assign_pc.service';
import { ProgrammeModel } from '../../../../models/programme';
import { UserProfile } from '../../../../models/userProfile.model';
import { ProfileService } from '../../../profile/components/profile.service';
import { Component, OnInit, OnChanges } from '@angular/core';

@Component({
    selector: 'assign-pc',
    templateUrl: './assign_pc.component.html',
})
export class AssignPCComponent implements OnInit {

    programmesList: ProgrammeModel[];
    myProfile: UserProfile[];
    staffList: StaffList[];
    myDepartmentPk: number;

    constructor(
        private assignPCService: AssignPCService,
        private profileService: ProfileService,
    ) {}

    ngOnInit() {
        this.assignPCService.getProgrammes()
        .subscribe(
            (listOfProgremmes: any) => {
                console.log('got programmes');
                console.log(listOfProgremmes);
                this.programmesList = listOfProgremmes;
                console.log(this.programmesList);
            },
            (error: any) => {
                console.log('explanation');
                console.log(error.response_explanation);

                // if (error.response_code === 'EMAIL_LINK_EXPIRED') {
                //     this.verificationLinkExpired = true;
                // } else if (error.response_code === 'BAD_VERIFICATION_LINK') {
                //     this.invalidLink = true;
                // }
            },
        );
      
        this.profileService.getMyProfile()
          .subscribe(
            (myProfile: any) => {
                console.log('got programmes');
                console.log(myProfile);
                this.myProfile = myProfile;
                console.log(this.myProfile);
                this.myDepartmentPk = myProfile.staff_profile.department;
                console.log(this.myDepartmentPk);
                this.getStaffsOfMyDepartment();
            },
            (error: any) => {
                console.log('explanation');
                console.log(error.response_explanation);

                // if (error.response_code === 'EMAIL_LINK_EXPIRED') {
                //     this.verificationLinkExpired = true;
                // } else if (error.response_code === 'BAD_VERIFICATION_LINK') {
                //     this.invalidLink = true;
                // }
            },
          );
      
             
    }
  
    getStaffsOfMyDepartment() {
      this.assignPCService.getStaffList(this.myDepartmentPk)
           .subscribe(
             (staffList: any) => {
                console.log('got programmes');
                console.log(staffList);
                this.staffList = staffList;
                console.log(this.staffList);
            },
            (error: any) => {
                console.log('explanation');
                console.log(error.response_explanation);

                // if (error.response_code === 'EMAIL_LINK_EXPIRED') {
                //     this.verificationLinkExpired = true;
                // } else if (error.response_code === 'BAD_VERIFICATION_LINK') {
                //     this.invalidLink = true;
                // }
            },
           );   
    }
  
    onChange(staff: StaffList, programme: ProgrammeModel) {
      console.log("selected = " + staff.user);
      console.log("Programme = " + programme.id)
    }
    
}
