import { Observable } from 'rxjs/Rx';
import { getProgrammes, getStaffOfGivenDepartment } from '../../../../../environments/api_urls';
import { LoginService } from '../../../login/login.service';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
@Injectable()
export class AssignPCService {

    constructor(
        private http: Http,
        private loginService: LoginService,
    ) { }

    getProgrammes() {
        const head = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '.concat(this.loginService.getAccessToken()),
        });
        const requestOptions = new RequestOptions({
            headers: head,
        });
        return this.http.get(getProgrammes, requestOptions)
            .map(this.handleData)
            .catch(this.handleError);
    }

    private handleData(result: any) {
        const body = result.json();
        // console.log(body);
        return body;
    }

    private handleError(error: Response) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        // const errMsg = (error.message) ? error.message :
        // error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        // console.error(errMsg); // log to console instead
        console.log(error);
        console.log(error.json());
        return Observable.throw(error.json());
    }

    getStaffList(departmentPk: number) {
      
      const head = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '.concat(this.loginService.getAccessToken()),
        });
        const requestOptions = new RequestOptions({
            headers: head,
        });
      
        return this.http.get(getStaffOfGivenDepartment.concat('?department_pk=').concat(departmentPk.toString()), requestOptions)
          .map(this.handleData)
          .catch(this.handleError);
    }

    assignPC(programmeId: number, staffId: number) {
        
    }
}
