import { Component } from '@angular/core';

@Component({
  selector: 'curriculum',
  template: `<router-outlet></router-outlet>`,
})
export class CurriculumComponent {

  constructor() {
  }
}
