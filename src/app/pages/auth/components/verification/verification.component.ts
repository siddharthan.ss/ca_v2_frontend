import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { VerificationService } from './verification.service';

import { Router } from '@angular/router';

@Component({
    selector: 'auth-verification',
    templateUrl: './verification.component.html',
    styleUrls: ['../../auth.scss'],
})
export class VerificationComponent implements OnInit {
    private verificationKey;

    private verificationLinkExpired: boolean;
    private emailValid: boolean;
    private alreadyVerified: boolean;
    private invalidLink: boolean;

    constructor(
        private route: ActivatedRoute,
        private verificationService: VerificationService, 
        private router: Router,
        ) { }

    ngOnInit() {
    
        this.verificationLinkExpired = false;
        this.emailValid = false;

    this.route
        .queryParams
        .subscribe(params => {
            this.verificationKey = params['verification_key'];
        });

        console.log('inside verification component');
        console.log(this.verificationKey);

        this.verificationService.verifyActivationKey(this.verificationKey)
        .subscribe(
            (response: ResponseData) => {
                                console.log('verification key got back');
                                console.log(response);
                                if (response.response_code === 'EMAIL_VERIFIED') {
                                    this.emailValid = true;
                                } else if (response.response_code === 'EMAIL_ALREADY_VERIFIED') {
                                    this.alreadyVerified = true;
                                }
                                },
            (error: ResponseData) => {
                                console.log('explanation');
                                console.log(error.response_explanation);

                                if (error.response_code === 'EMAIL_LINK_EXPIRED') {
                                    this.verificationLinkExpired = true;
                                } else if (error.response_code === 'BAD_VERIFICATION_LINK') {
                                    this.invalidLink = true;
                                }
                            },
        );
        
    // do something with this.verification_key
}

private redirectToLoginPage() {
    this.router.navigateByUrl('/login');
}
}
