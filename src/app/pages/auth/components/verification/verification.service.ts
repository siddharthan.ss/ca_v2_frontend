import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Injectable } from '@angular/core';

import { verifyEmailURL } from '../../../../../environments/api_urls';
import { Observable } from 'rxjs/Rx';
@Injectable()
export class VerificationService {

    private verificationURL = verifyEmailURL;

    constructor(public http: Http) { }

    verifyActivationKey(verificationKey: string): Observable<ResponseData> {
        const data = {
            'verification_key': verificationKey,
        };
        const body = JSON.stringify(data);
        const head = new Headers({
            'Content-Type': 'application/json',
        });
        const requestOptions = new RequestOptions({
            headers: head,
        });

        return this.http.post(verifyEmailURL, body, requestOptions)
            .map(this.handleData)
            .catch(this.handleError);
    }

    private handleData(result: Response): Observable<ResponseData> {
        const body = result.json();
        console.log(body);
        return body;
    }

    private handleError(error: Response): Observable<ResponseData> {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        // const errMsg = (error.message) ? error.message :
        // error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        // console.error(errMsg); // log to console instead
        console.log(error);
        console.log(error.json());
        return Observable.throw(error.json());
    }
}

