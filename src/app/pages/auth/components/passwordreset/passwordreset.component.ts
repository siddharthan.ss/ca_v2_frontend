import { Component } from '@angular/core';
import {PasswordResetService} from './passwordreset.service';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'user',
  templateUrl: './passwordreset.component.html',
  providers: [PasswordResetService],
  styleUrls: ['../../auth.scss'],
})
export class PasswordResetComponent  {
  // form: FormGroup;
  email:string;
  // msg:Pass;
  submitted:boolean;
  errmsg:boolean;
  res;

  constructor(private passService: PasswordResetService){
        this.submitted=false;
        this.errmsg=false;
        this.email='gct@gct.ac.in';
    console.log('inside constructor');


  }
  Resetpass(email:string)
{
    console.log(email);
    console.log('inside reset pass');
  this.passService.getPass(email).subscribe(res=> {
    this.submitted=true;
    console.log(res);
    console.log(res['response_code']);
    console.log(res['response_message']);
    console.log(res['response_explanation']);
    this.res = res;
    if(this.res.response_code=="EMAIL_DOES_NOT_EXIST")
    {
      this.submitted = false;
      this.errmsg =true;
      console.log('email invalid');
    }
    // console.log(this.posts.response_code);


  });
}

}
interface Pass{
  msg: string;
}

interface Resp
{
  response_code:string;
  response_message:string;
  response_explanation:string;
}


