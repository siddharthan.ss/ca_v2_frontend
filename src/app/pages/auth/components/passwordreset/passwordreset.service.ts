import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Headers, Http, Response, URLSearchParams } from '@angular/http';

@Injectable()
export class PasswordResetService {
  private ResetPasswordURL = 'http://127.0.0.1:8002/api/accounts/password_reset/';
  constructor(private http: Http) {
    console.log('PostsService Initialized...');
  }

  getPass(email: string): Observable<JSON> {
  const body = 'email=' + email ;

  const headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });

  return this.http.post(this.ResetPasswordURL , body, {
      headers,
    }).map(this.handleData)
      .catch(this.handleError);
  }

  private handleData(res: Response) {
    const body = res.json();
    return body;
  }

  private handleError (error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(error);
}
  }


