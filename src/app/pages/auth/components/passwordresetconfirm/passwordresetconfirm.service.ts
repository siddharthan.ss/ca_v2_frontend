import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Headers, Http, Response, URLSearchParams } from '@angular/http';

@Injectable()
export class ConfirmPasswordResetService {
  private VerifyResetpasswordURL = 'http://127.0.0.1:8000/api/accounts/password_reset_verify/';
  private ConfirmResetpasswordURL = 'http://127.0.0.1:8000/api/accounts/password_reset_confirm/';
  public reset_key: string;
  constructor(private http: Http) {
    console.log('Confirm Password Reset Service Initialized...');
  }

  LinkVerify(reset_key): Observable<JSON> {
    console.log('inside get pass');
    console.log(reset_key);
  const body = 'reset_key=' + reset_key ;

  const headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });

  return this.http.post(this.VerifyResetpasswordURL , body, {
      headers,
    }).map(this.handleData)
      .catch(this.handleError);
  }


  ResetPassword(reset_key,pass1,pass2): Observable<JSON> {
    console.log('inside get ResetPassword');

    const body = 'reset_key=' + reset_key+'&password='+pass1+ '&confirm_password='+pass2;
    console.log(body);

    const headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });

    return this.http.post(this.ConfirmResetpasswordURL , body, {
      headers,
    }).map(this.handleData)
      .catch(this.handleError);
  }

  private handleData(res: Response) {
    const body = res.json();
    return body;
  }

  private handleError (error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(error);
}
  }


