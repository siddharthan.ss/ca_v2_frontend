import { Component, OnInit } from '@angular/core';
import {ConfirmPasswordResetService} from './passwordresetconfirm.service';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import {ActivatedRoute} from "@angular/router";


@Component({
  selector: 'user',
  templateUrl: './passwordresetconfirm.component.html',
  providers: [ConfirmPasswordResetService],
  styleUrls: ['../../auth.scss'],
})
export class ConfirmPasswordResetComponent implements OnInit {
  // form: FormGroup;
  msg:Pass;
  submitted:boolean;
  errmsg:boolean;
  errmsg1:boolean;
  res;
  res1;
  pass1;
  pass2;
  reset_key;


  constructor(private route: ActivatedRoute,
    private passService: ConfirmPasswordResetService,
){

        this.submitted=false;
        this.errmsg=false;
        this.errmsg1=false;
    console.log('inside constructor');
  }
  ngOnInit() {
    // Capture the access token and code
    this.route
      .queryParams
      .subscribe(params => {
        this.reset_key = params['reset_key'];
      });

    console.log('reset key');
    console.log(this.reset_key);
    console.log(this.reset_key);

    this.passService.LinkVerify(this.reset_key).subscribe(res=> {

      console.log(res);
      console.log(res['response_code']);
      console.log(res['response_message']);
      console.log(res['response_explanation']);
      this.res = res;
      if(this.res.response_code=="LINK_INVALID"||this.res.response_code=="LINK_EXPIRED"||this.res.response_code=="FALSE_LINK")
      {
        this.errmsg=true;
      }
      else
      {
        this.errmsg=false;
      }



        });


  }
  Resetpassconfirm()
{
    console.log('inside Resetpassconfirm()');
  this.passService.ResetPassword(this.reset_key,this.pass1,this.pass2).subscribe(res1=> {
    this.submitted=true;
    console.log(res1);
    console.log(res1['response_code']);
    console.log(res1['response_message']);
    console.log(res1['response_explanation']);
    this.res = res1;
    if(this.res1.response_code=="DIFFERENT_PASSWORD")
    {
      this.errmsg1=true;
    }
    else {
      this.errmsg1=false;
      this.submitted=true;


    }
    console.log(this.errmsg1);


  });

}

}
interface Pass{
  msg: string;
}

interface Resp
{
  response_code:string;
  response_message:string;
  response_explanation:string;
}



