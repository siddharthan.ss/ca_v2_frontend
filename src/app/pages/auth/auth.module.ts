import { CommonModule } from '@angular/common';
import { declaredViewContainer } from '@angular/core/src/view/util';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { routing } from './auth.routing';

import { VerificationComponent } from './components/verification/verification.component';
import { AuthComponent } from 'app/pages/auth/auth.component';

import { VerificationService } from './components/verification/verification.service';
import { PasswordResetComponent } from './components/passwordreset/passwordreset.component';
import { PasswordResetService } from './components/passwordreset/passwordreset.service';
import { AppTranslationModule } from '../../app.translation.module';
import { NgaModule } from '../../theme/nga.module';
import { ConfirmPasswordResetComponent } from './components/passwordresetconfirm/passwordresetconfirm.component';
import { ConfirmPasswordResetService } from './components/passwordresetconfirm/passwordresetconfirm.service';

@NgModule({
    imports: [
        CommonModule,
        routing,
        FormsModule,
        AppTranslationModule,
        ReactiveFormsModule,
        NgaModule,
    ],
    declarations: [
        VerificationComponent,
        PasswordResetComponent,
        ConfirmPasswordResetComponent,
        AuthComponent,
    ],
    providers: [
        VerificationService,
        PasswordResetService,
        ConfirmPasswordResetService,
    ],
})
export class AuthModule {}
