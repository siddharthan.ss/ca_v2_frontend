import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthComponent } from './auth.component';
import { VerificationComponent } from './components/verification/verification.component';
import { PasswordResetComponent } from './components/passwordreset/passwordreset.component';
import {ConfirmPasswordResetComponent} from "./components/passwordresetconfirm/passwordresetconfirm.component";

const routes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      { path: 'verification', component: VerificationComponent },
      { path: 'passwordreset', component: PasswordResetComponent },
      { path: 'confirmpasswordreset', component: ConfirmPasswordResetComponent },
    //   { path: 'verification', component: VerificationComponent },
    ],
  },
];

export const routing = RouterModule.forChild(routes);
