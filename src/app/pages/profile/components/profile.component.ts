import { ProfileService } from './profile.service';
import { AnimationTransitionEvent, Component, OnInit } from '@angular/core';


@Component({
    selector: 'profile-sidebar',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.scss'],
})

export class ProfileComponent implements OnInit {
  
   constructor(
     private profileService: ProfileService,
   ) {}
   ngOnInit() {
     this.profileService.getMyProfile().
     subscribe(
       (myProfile: any) => {
                console.log('got my profile');
                console.log(myProfile);
            },
            (error: any) => {
                console.log('explanation');
                console.log(error.response_explanation);

                // if (error.response_code === 'EMAIL_LINK_EXPIRED') {
                //     this.verificationLinkExpired = true;
                // } else if (error.response_code === 'BAD_VERIFICATION_LINK') {
                //     this.invalidLink = true;
                // }
            },
     );
   }
}
