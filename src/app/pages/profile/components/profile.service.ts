import { getMyProfileURL } from '../../../../environments/api_urls';
import { UserProfile } from '../../../models/userProfile.model';
import { LoginService } from '../../login/login.service';
import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs';

@Injectable()
export class ProfileService {

  constructor(
    private _http: Http,
    private loginService: LoginService,
  ) { }
  
  getMyProfile(): Observable<UserProfile> {
    
    const head = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '.concat(this.loginService.getAccessToken()),
        });
        const requestOptions = new RequestOptions({
            headers: head,
        });
    
    return this._http.get(getMyProfileURL, requestOptions)
      .map(this.handleData)
      .catch(this.handleError);
  }
  
  private handleData(result: any) {
        const body = result.json();
        // console.log(body);
        return body;
    }
  
  private handleError(error: Response) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        // const errMsg = (error.message) ? error.message :
        // error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        // console.error(errMsg); // log to console instead
        console.log(error);
        console.log(error.json());
        return Observable.throw(error.json());
    }

}
