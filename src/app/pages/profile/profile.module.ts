import { LoginService } from '../login/login.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppTranslationModule } from '../../app.translation.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';

import { routing } from './profile.routing';
import { ProfileComponent } from './components/profile.component';
import { ProfileService } from './components/profile.service';


@NgModule({
  imports: [
    CommonModule,
    AppTranslationModule,
    ReactiveFormsModule,
    FormsModule,
    NgaModule,
    routing,
  ],
  declarations: [
    ProfileComponent,
  ],
  providers: [
    ProfileService,
    LoginService,
  ],
})
export class ApprovalModule {}
