import { ProfileComponent } from './components/profile.component';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

// noinspection TypeScriptValidateTypes
export const routes: Routes = [
  {
    path: '',
    component: ProfileComponent,
  },
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
