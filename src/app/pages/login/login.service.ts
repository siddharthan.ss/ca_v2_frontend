import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { loginURL, loginUtilURL, resendVerificationEmailURL } from '../../../environments/api_urls';

@Injectable()
export class LoginService {
  private oauthLoginEndPointUrl = loginURL;  // Oauth Login EndPointUrl to web API
  private clientId = 'ivZknPQu8cnsSFwkoO89gl7dAMkLtiW63Z6QDPs1';
  private clientSecret = 'KCoPiJWQfUOlDGgafRHGWzaOlnnB34SNoMxcvUU1WRssGw78dFyxmXl6JQFLpGLx8y685XGeVQm8jj1LPU6asKzTeVcSEnkwF9LMtWrqmyJpk6SHg84eVCHJr66RPeLq';

  constructor(public http: Http) { }

  login(username: string, password: string): Observable<JSON> {


    const body = 'grant_type=password&username=' + username + '&password=' + password + '&client_id=' + this.clientId + '&client_secret=' + this.clientSecret;

    const headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    return this.http.post(this.oauthLoginEndPointUrl, body, {
      headers,
    }).map(this.handleData)
      .catch(this.handleError);
  }

  private handleData(res: Response) {
    const body = res.json();
    return body;
  }

  private handleError(error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(error);
  }

  loginUtilCheck(accessToken: string): Observable<ResponseData> {
    const head = new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '.concat(accessToken),
    });
    const requestOptions = new RequestOptions({
      headers: head,
    });

    return this.http.get(loginUtilURL, requestOptions)
      .map(this.handleUtilData)
      .catch(this.handleUtilError);
  }

  private handleUtilData(result: Response): Observable<ResponseData> {
    const body = result.json();
    console.log(body);
    return body;
  }

  private handleUtilError(error: Response): Observable<ResponseData> {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    // const errMsg = (error.message) ? error.message :
    // error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    // console.error(errMsg); // log to console instead
    console.log(error);
    console.log(error.json());
    return Observable.throw(error.json());
  }

  resendVerificationLink(accessToken: string): Observable<ResponseData> {
    const head = new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '.concat(accessToken),
    });
    const requestOptions = new RequestOptions({
      headers: head,
    });

    return this.http.get(resendVerificationEmailURL, requestOptions)
      .map(this.handleUtilData)
      .catch(this.handleUtilError);
  }

  getMyAccountApprover(accessToken: string): Observable<ResponseData> {
    const head = new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '.concat(accessToken),
    });
    const requestOptions = new RequestOptions({
      headers: head,
    });

    return this.http.get(loginURL, requestOptions)
      .map(this.handleUtilData)
      .catch(this.handleUtilError);
  }

  getAccessToken() {
    // todo handle refresh token
    return localStorage.getItem('token');
  }

  logout() {
    localStorage.removeItem('token');
  }
}
