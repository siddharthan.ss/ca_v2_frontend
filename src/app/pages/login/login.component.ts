import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from 'app/pages/login/login.service';

@Component({
  selector: 'login',
  templateUrl: './login.html',
  styleUrls: ['./login.scss'],
})
export class Login {

  form: FormGroup;
  email: AbstractControl;
  password: AbstractControl;
  submitted: boolean = false;

  accessToken: string;
  emailNotVerified: boolean = false;
  emailResent: boolean = false;
  alreadyVerified: boolean = false;
  accountNotApproved: boolean = false;


  constructor(
    fb: FormBuilder,
    private loginService: LoginService,
    private router: Router,
  ) {
    this.form = fb.group({
      'email': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'password': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
    });

    this.email = this.form.controls['email'];
    this.password = this.form.controls['password'];
  }

  onSubmit(values: Object): void {
    this.submitted = true;
    if (this.form.valid) {
      // your code goes here
      // console.log(values);

      this.loginService.login(this.email.value, this.password.value)
        .subscribe(
        (response: any) => {
          this.accessToken = response.access_token;
          this.checkAndLogin();
        },
        (error: any) => {
          if (error.status === 401) {
            alert('Invalid login credentials');
          } else {
            console.log('Some other error');
          }
        },
      );
    }
  }
  storeAccessTokenLocally(accessToken: string) {
    localStorage.setItem('token', accessToken);
  }


  checkAndLogin() {

    this.loginService.loginUtilCheck(this.accessToken)
      .subscribe(
      (response: ResponseData) => {
        console.log(response);

        if (response.response_code === 'CAN_LOGIN') {
          this.storeAccessTokenLocally(this.accessToken);
          this.router.navigateByUrl('pages/dashboard');
        } else if (response.response_code === 'NOT_FILLED_BIO_DATA') {
          this.storeAccessTokenLocally(this.accessToken);
          this.router.navigateByUrl('bio');
        }


      },
      (error: ResponseData) => {
        console.log('explanation');
        console.log(error.response_explanation);

        if (error.response_code === 'EMAIL_NOT_VERIFIED') {
          this.emailNotVerified = true;
          this.emailResent = false;
          this.alreadyVerified = false;
          this.accountNotApproved = false;
        } else if (error.response_code === 'ACCOUNT_NOT_APPROVED') {
          this.emailNotVerified = false;
          this.emailResent = false;
          this.alreadyVerified = false;
          this.accountNotApproved = true;
          this.setMyAccountApprover();
        }
      },
    );

  }

  // todo
  setMyAccountApprover() {
    this.loginService.getMyAccountApprover(this.accessToken)
      .subscribe(
      (response: ResponseData) => {

      },
      (error: ResponseData) => {

      },
    );
  }
  resendVerificationMail() {
    this.loginService.resendVerificationLink(this.accessToken)
      .subscribe(
      (response: ResponseData) => {
        if (response.response_code === 'VERIFICATION_LINK_RESENT') {
          this.emailNotVerified = false;
          this.emailResent = true;
          this.alreadyVerified = false;
          this.accountNotApproved = false;
        }
      },
      (error: ResponseData) => {
        if (error.response_code === 'EMAIL_ALREADY_VERIFIED') {
          this.emailNotVerified = false;
          this.emailResent = false;
          this.alreadyVerified = true;
          this.accountNotApproved = false;
        }
      },
    );
  }
}
