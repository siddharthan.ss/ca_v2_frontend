// this file contains  the URL's to access the backend server


export const baseURL: string = 'http://localhost:8000/';
// export const baseURL: string = 'http://localhost:8002/';

export const loginURL: string = baseURL.concat('o/token/');
export const registerURL: string = baseURL.concat('api/accounts/custom_user/create/');
export const verifypasswordresetURL = baseURL.concat('api/accounts/password_reset_verify/');
export const ConfirmResetpasswordURL = baseURL.concat('api/accounts/password_reset_confirm/');
export const PasswordEmailURL = baseURL.concat('api/accounts/password_reset/');
export const verifyEmailURL = baseURL.concat('api/accounts/verify_email/');
export const resendVerificationEmailURL = baseURL.concat('api/accounts/resend_verification_link/');
export const loginUtilURL = baseURL.concat('api/accounts/login_util/');
export const StaffCreateURL = baseURL.concat('api/accounts/staff/create/');
export const StudentCreateURL = baseURL.concat('api/accounts/student/create/');
export const SelfURL = baseURL.concat('api/accounts/self/');
export const getStaffsToApproveURL = baseURL.concat('api/accounts/get_staffs_to_approve/');
export const approveCustomUserURL = baseURL.concat('api/accounts/approve_disapprove_users/');
export const DepartmentSectionURL = baseURL.concat('api/accounts/dsgetall/');
export const getProgrammes = baseURL.concat('api/accounts/programme/');
export const getMyProfileURL = baseURL.concat('api/accounts/get_my_profile/');

export const getStaffOfGivenDepartment = baseURL.concat('api/curriculum/get_staffs_of_given_department');
